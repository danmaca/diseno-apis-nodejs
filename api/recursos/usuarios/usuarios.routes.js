const express = require('express')
const _ = require('underscore')
const uuidv4 = require('uuid/v4')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const log = require('../../../utils/logger')
const validarUsuario = require('./usuarios.validate').validarUsuario
const validarPedidoDeLogin = require('./usuarios.validate').validarPedidoDeLogin
const config = require('../../../config')
const usuarioController = require('./usuarios.controller')
const procesarErrores = require('../../libs/errorHandler').procesarErrores
const { DatosDeUsuarioYaEnUso, CredencialesIncorrectas } = require('./usuarios.error')

const usuariosRouter = express.Router()

function transformarBodyALowercase(req, res, next) {
  req.body.username && (req.body.username = req.body.username.toLowerCase())
  req.body.email && (req.body.email = req.body.email.toLowerCase())
  next()
}

usuariosRouter.get('/', procesarErrores((req, res) => {
  return usuarioController.obtenerUsuarios()
    .then(usuarios => {
      res.json(usuarios)
    })
}))

usuariosRouter.post('/', [validarUsuario, transformarBodyALowercase], procesarErrores((req, res) => {
  let nuevoUsuario = req.body

  return usuarioController.usuarioExiste(nuevoUsuario.username, nuevoUsuario.email)
    .then(usuarioExiste => {
      if (usuarioExiste) {
        log.warn(`Email [${nuevoUsuario.email}] o username [${nuevoUsuario.username}] ya existen en la base de datos`)
        throw new DatosDeUsuarioYaEnUso()
      }

      return bcrypt.hash(nuevoUsuario.password, 10)
    })
    .then((hash) => {
      return usuarioController.crearUsuario(nuevoUsuario, hash)
        .then(nuevoUsario => {
          res.status(201).send('Usuario creado exitósamente.')
        })
    })
}))

usuariosRouter.post('/login', [validarPedidoDeLogin, transformarBodyALowercase], procesarErrores(async (req, res) => {
  let usuarioNoAutenticado = req.body
  
  
  let usuarioRegistrado = await usuarioController.obtenerUsuario({ username: usuarioNoAutenticado.username })
  if (!usuarioRegistrado) { 
    log.info(`Usuario [${usuarioNoAutenticado.username}] no existe. No pudo ser autenticado`)
    throw new CredencialesIncorrectas()
  }

  let contraseñaCorrecta = await bcrypt.compare(usuarioNoAutenticado.password, usuarioRegistrado.password)
  if (contraseñaCorrecta) {
    let token = jwt.sign({ id: usuarioRegistrado.id }, config.jwt.secreto, { expiresIn: config.jwt.tiempoDeExpiración })
    log.info(`Usuario ${usuarioNoAutenticado.username} completo autenticación exitosamente.`)
    res.status(200).json({ token })
  } else {
    log.info(`Usuario ${usuarioNoAutenticado.username} no completo autenticación. Contraseña incorrecta`)
    throw new CredencialesIncorrectas()
  }
}))

module.exports = usuariosRouter